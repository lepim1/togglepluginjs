/**
*	HTML toggle events
*/
$( document ).ready(function() {    	
	$("button").click(function() {
		if($(this).data("toggle-trigger")) {
			//Gets the target selector
			var target = $("#" + $(this).data("toggle-trigger"));
			
			//Call the appropriated function acording to the attribute data-toggle 
			if (target.data("toggle") == "toggleClass") {
				togglePlugin.toggleClass(target, target.data("class1"), target.data("class2"));
			} else {
				if (target.data("toggle") == "toggleDisabled") {
					togglePlugin.toggleDisabled(target);
				} else {
					if (target.data("toggle") == "toggleAttribute") {
						togglePlugin.toggleAttribute(target, target.data("attr"), target.data("value1"), target.data("value2"));
					}
				}
			}
		}		
	});
});

/**
*	Core functions 
*/
function TogglePlugin(){
	
	// Toogle between class1 and class2 for a HTML selector
	this.toggleClass = function(selector, class1, class2, callback) {
		if($(selector).length != 0){
			// If current class is class1, replace for class2
			if($(selector).hasClass(class1)) {		
				$(selector).removeClass(class1).addClass(class2);
				if(callback && callback.onClass2 && typeof callback.onClass2 === "function") {
					callback.onClass2.call(this);
				}
			} else { 
				// If current class is class2, replace for class1
				$(selector).removeClass(class2).addClass(class1);		
				if(callback && callback.onClass1 && typeof callback.onClass1 === "function") {
					callback.onClass1.call(this);
				}
			}
		}
	};
	
	// Toogle attribute between value1 and value2 for a HTML selector
	this.toggleAttribute = function(selector, attribute, value1, value2, callback) {
		if($(selector).length != 0){
			// If current attribute has the value1, replace it for value2
			if($(selector).attr(attribute) == value1 ) {
				$(selector).attr(attribute, value2);
				if(callback && callback.onValue2 && typeof callback.onValue2 === "function") {
					callback.onValue2.call(this);
				}
			} else {
				// If current attribute has the value2, replace it for value1
				$(selector).attr(attribute, value1);
				if(callback && callback.onValue1 && typeof callback.onValue1 === "function") {
					callback.onValue1.call(this);
				}
			}
		}
	};
	
	// Toogle attribute disabled/enabled for a HTML selector
	this.toggleDisabled = function(selector, callback) {
		if($(selector).length != 0){
			// If element is disabled, enable it
			if($(selector).prop("disabled")) {
				$(selector).removeAttr("disabled");
				if(callback && callback.onEnabled && typeof callback.onEnabled === "function") {
					callback.onEnabled.call(this);
				}
			} else {
				// If element is enabled, disable it
				$(selector).attr("disabled","disabled");
				if(callback && callback.onDisabled && typeof callback.onDisabled === "function") {
					callback.onDisabled.call(this);
				}
			}
		}
	};
}

// Initialize the toggle plugin
var togglePlugin = new TogglePlugin();